﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace FacturaWCF.Models
{
    [DataContract]
    public class GetBillResponseMessage
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public IEnumerable<BillInformation> Bills { get; set; }
    }
}