﻿using System;
using System.Runtime.Serialization;

namespace FacturaWCF.Models
{
    [DataContract]
    public class BillInformation
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime ModifiedOn { get; set; }
        [DataMember]
        public DateTime CreatedOn { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int CustomerId { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string CustomerAddress { get; set; }
        [DataMember]
        public string CustomerEmail { get; set; }
        [DataMember]
        public string ErrorMesaje { get; set; }
    }
}