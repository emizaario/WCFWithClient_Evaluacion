﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using FacturaWCF.Models;

namespace FacturaWCF
{
    [ServiceContract]
    public interface IBillService
    {
        [OperationContract]
        //  [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "GetBIllByPage/{page}")]
        GetBillResponseMessage GetBIllByPage(int page);
    }
}
