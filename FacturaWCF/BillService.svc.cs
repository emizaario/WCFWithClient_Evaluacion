﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Helpers;
using FacturaWCF.Models;

namespace FacturaWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Bill" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Bill.svc o Bill.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Bill : IBillService
    {
        public GetBillResponseMessage GetBIllByPage(int page)
        {
            try
            {
                var theBillInformations = GetBills(page);

                return new GetBillResponseMessage {Success = true, Bills = theBillInformations};
            }
            catch (Exception ex)
            {
              return new GetBillResponseMessage {Success = false, ErrorMessage = ex.Message};
            }
            
        }

        private static IEnumerable<BillInformation> GetBills(int page)
        {
            var theBills=new List<BillInformation>();

            var conn =
                new SqlConnection(
                    System.Configuration.ConfigurationManager.ConnectionStrings["FacturaWCFDB"].ConnectionString);
            using (var cmd = new SqlCommand("sp_GetBillByPage", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@page", page));

                conn.Open();
                var rdr = cmd.ExecuteReader();
                
                while (rdr.Read())
                {
                    var theBill= new BillInformation
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]),
                        ModifiedOn = Convert.ToDateTime(rdr["ModifiedOn"]),
                        CustomerId = Convert.ToInt32(rdr["CustomerId"]),
                        CustomerName = rdr["CustomerName"].ToString(),
                        CustomerAddress = rdr["CustomerAddress"].ToString(),
                        CustomerEmail = rdr["CustomerEmail"].ToString(),
                        Description = rdr["Description"].ToString()
                       
                    };

                    theBills.Add(theBill);
                }

                conn.Close();
            }

            return theBills;
        }
    }
   
}
