﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCFBillClient.BillServiceWCF;
using WCFBillClient.Models;

namespace WCFBillClient.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int page = 1)
        {

            page = ValidatePage(page);
            using (var theService = new BillServiceClient())
            {
                var response = theService.GetBIllByPage(page);

                var theBillsList = response.Bills.Select(bill => new BillInformationModel
                {
                    CreatedOn = bill.CreatedOn,
                    Id = bill.Id,
                    ModifiedOn = bill.ModifiedOn,
                    Description = bill.Description,
                    CustomerId = bill.CustomerId,
                    CustomerName = bill.CustomerName,
                    CustomerAddress = bill.CustomerAddress,
                    CustomerEmail = bill.CustomerEmail
                }).ToList();

                Session["theBillsList"] = theBillsList;

                ViewBag.currentpage = page;

                return View(theBillsList);
            }


        }

        public ActionResult Details(int id)
        {
            var theBillsList = (IEnumerable<BillInformationModel>)Session["theBillsList"];
            return View(theBillsList.FirstOrDefault(x => x.Id == id));
        }

        private int ValidatePage(int currrentPage)
        {
            return currrentPage <= 0 ? 1 : currrentPage;
        }
    }
}