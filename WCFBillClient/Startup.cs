﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WCFBillClient.Startup))]
namespace WCFBillClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
